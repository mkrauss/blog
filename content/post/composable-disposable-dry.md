+++
title = "Composable, Disposable, DRY"
author = ["Matthew Krauss"]
date = 2022-02-13T17:17:00-05:00
lastmod = 2022-02-13T20:47:16-05:00
draft = false
weight = 1001
+++

There's a growing current of thought out there in opposition to the
classic _DRY_ (Don't Repeat Yourself) principal.

First, there was [The Wrong Abstraction](https://sandimetz.com/blog/2016/1/20/the-wrong-abstraction) from Sandi Metz, discussing the
benefits of repeating code, rather than writing the wrong
abstraction - "prefer duplication over the wrong abstraction" - and I
agree with that. I've done it myself, and believe that it is a good
choice, in those cases where we are struggling to find the right
abstraction; however, those cases should be rare, and refactoring once
we have more understanding should be a high priority. She also
discusses the strategy of re-introducing duplication, when we've found
that we have a wrong abstraction, stepping back before we can find the
right abstraction and move forward again, which, though tedious, can
be the best way forward when we've made a mistake. There's a lot of
good in this article. However, at one point, she gives a scenario in
which:

> Programmer B feels honor-bound to retain the existing abstraction, but
> since isn't exactly the same for every case, they alter the code to
> take a parameter, and then add logic to conditionally do the right
> thing based on the value of that parameter.

This sets the stage for what we will see in the other articles.

Next came [AHA Programming](https://kentcdodds.com/blog/aha-programming) from Kent C. Dodds, who, inspired by Ms.
Metz, suggests that we should "Avoid Hasty Abstractions". Mr. Dodds
makes quite the interesting statement:

> If you abstract early though, you'll think the function or component
> is perfect for your use case and so you just bend the code to fit your
> new use case. This goes on several times until the abstraction is
> basically your whole application in if statements and loops 😂😭

In this scenario, the cost of premature abstraction is suggested to
be, essentially, the same as the cost of never abstracting! And that
can be true, if we understand abstraction to be folding additional
logic into an existing function with conditional and loop structures.

Then we have [Its OK to repeat yourself](https://gomakethings.com/its-ok-to-repeat-yourself-dry-coding-isnt-the-holy-grail/) from Chris Ferdinandi, who
gives some concrete code examples, where he demonstrates reducing code
duplication by combining functions, and concludes:

> By the time you account for all of these differences, you end up with
> an abstracted function that’s more bloated and less usable than just
> having two functions.

Of these three articles, this is the only one to give code samples. I
could be wrong, but I think the samples he gives are in line with what
the other two authors are thinking of. I suggest reading all three
articles before coming back here, and keeping Mr. Ferdinandi's article
open in another tab for reference, as I will be directly addressing
his code samples, and refactoring them.

All these linked articles, at some point, refer to removing repitition
by creating an "abstraction" which is just a function with extra
parameters and conditional logic, and suggest that it may be better to
leave the repetition. They are, in avoiding abstraction, solving the
wrong problem. The problem isn't the risk of choosing the wrong
abstraction; the problem is a fundamental **misunderstanding of what
abstraction is**. A function which takes special parameters and
conditionally executes different logic to handle different uses is not
abstract; it is very concrete, and written to concretely accomplish
each of a defined, concrete collection of tasks, to serve multiple
purposes. Remember another basic guideline: the _Single Reponsibility
Principal_ - combining multiple tasks into one function or class is
not good abstraction.

<div class="note">

Abstract code should express less explicit logic, achieving
flexibility and reusability by being intentionally unaware of how it
is used, rather than by being coupled to every use.

</div>

As we break our code into smaller, composable pieces, we find the
right abstractions naturally and effortlessly. As requirements change,
some of our composable pieces may become obsolete, and that's fine.
They're small, simple, and disposable.

These are the watchwords: composable and disposable.

Introducing his first example of abstracting, what he proposes as a
good abstraction, he says:

> We can add a second property to our contentList() function, ordered.
> If true, we’ll return an ordered list. If not, we’ll return an
> unordered list instead.

Do you see the red flags already? To "add a second property"; to do
one thing or another "if" some condition is true. This strategy is
combining concrete execution paths into one function to eliminate
repetition, but not really abstracting at all. Take a look at the
[_DRY_ version](https://gomakethings.com/its-ok-to-repeat-yourself-dry-coding-isnt-the-holy-grail/#making-functions-dry) - there's less code, but it's more complicated, and only
handles conditions the author has already thought of.

Instead of combining functions with different purposes, we can
identify the common purpose of the shared pieces of code, and extract
that out - in this case, creating the HTML to represent the items of a
list:

```js
/**
 * Create a list of linked content items
 *
 * @param  {Array}  items  The content
 * @return {String}        The content list HTML
 */
function orderedList (items) {
  return `<ol>${listItems(items)}</ol>`;
}

/**
 * Create a list of linked content items
 *
 * @param  {Array}  items  The content
 * @return {String}        The content list HTML
 */
function unorderedList (items) {
  return `<ul>${listItems(items)}</ul>`;
}

/**
 * Create a list of linked content items
 *
 * @param  {Array}   items   The content
 * @return {String}          The content list items HTML
 */
function listItems (items) {
  return items.map(function (item) {
    return `<li id="${item.id}"><a class="link-no-underline" href="${item.url}">${item.title}</a></li>`;
  }).join('');
}
```

For the next example, the [bad abstraction](https://gomakethings.com/its-ok-to-repeat-yourself-dry-coding-isnt-the-holy-grail/#too-much-abstraction-is-bad), Mr. Ferdinandi talks about
how many steps two original functions have in common to accomplish
their purposes, and then says:

> But they have a handful of small differences that make additional
> abstraction worse than just having two functions.

What he doesn't discuss is that these two functions have very
different purposes. I certainly agree that they should be separate
functions, regardless of whether they have common code, even if their
code was (coincidentally) exactly the same! Common code does not mean
common purpose. The art of abstracting is to find smaller,
higher-level or more general purposes, and to extract complex pieces
of code into simpler, easier-to-understand functions or classes to
accomplish those purposes. In this case, somebody else has already
added the well-known `thenable` abstraction to the `fetch` API, which
can do a lot of the lifting for us.

```js
/**
 * Make an API call which assumes the result is in JSON and implements error
 * handling that is common to our code
 *
 * @param {String} path    The path within the API to fetch
 * @param {Object} options The fetch options
 * @return {Promise}       The fetch promise
 */
function ourFetch(path, options = {}) {
  return fetch(path, options)
    .then(function (response) {
      return response.json()
    })
    .then(function (data) {
      if (data.code >= 200 && data.code < 300) return data;
      throw data;
    });
}

/**
 * Make a call to the courses API
 * @param  {Object}  data  Data to send with the API [optional]
 * @return {Promise}       The fetch Promise
 */
function callAPI (data) {
  return ourFetch('/account-endpoint', {
    method: 'POST',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-type': 'application/x-www-form-urlencoded'
    },
    body: buildQuery(data)
  });
}

/**
 * Get data for a product
 * @return {Promise} The fetch Promise
 */
function getProductData (api) {
  return ourFetch(`/product-endpoint&api=${api}`)
    .catch(function (error) {
      if (error.code === 401) {
        logoutUser();
      }
    });
}
```

Notice that instead of adding parameters and conditional logic, we are
splitting behavior into smaller, reusable pieces, to combine them as
we need. **This is just a start** - that `ourFetch` function name should
be a bit of a red flag, as it is such an arbitrary way to distinguish
what we're doing here; if we look closer, we're actually doing a
couple things. This function isn't as disposable as we might hope,
since other functions have hard dependencies on it.

There's certainly more possible improvements as well. I've recently
added Genius annotations to my blog, so I'd love to see some
annotations added to share how you would do it!

Composable, disposable, _DRY_.