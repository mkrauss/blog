+++
title = "Emacs is a lisp machine"
author = ["Matthew Krauss"]
date = 2013-01-24T01:28:00-05:00
lastmod = 2022-02-13T18:09:38-05:00
tags = ["emacs", "lisp"]
draft = false
weight = 1008
+++

I posted the following to <http://c2.com/cgi/wiki?EmacsVsVi> in response
to two comments about the size of Emacs and it's imagined opposition
to the Unix philosophy, sometime in late 2011, and am archiving here
for posterity:

What a lot of people fail to understand about Emacs is that it is not
a large text editor.

Emacs is a small Lisp machine. "Emacs" also commonly refers to a
reasonably small text editor which runs on that Lisp machine, and
serves as it's primary user interface. "Emacs" can also be used to
refer collectively to the Lisp machine, the editor, and the entire
massive collection of software which runs on it - mostly small
programs that do one thing and do it well.

This is no more opposed to the Unix philosophy than Unix (or Linux)
itself, which refers to a kernel, a (variety of) command shell(s), and
an entire massive collection of software which runs on it.