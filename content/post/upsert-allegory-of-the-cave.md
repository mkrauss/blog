+++
title = "The Upsert Allegory of the Cave"
author = ["Matthew Krauss"]
date = 2019-11-22T11:03:00-05:00
lastmod = 2022-02-13T18:09:26-05:00
tags = ["sql"]
draft = false
weight = 1004
+++

I have in my hand a shiny new fact.

When the fact in my hand might contradict the facts in the database:

-   If I know that my fact is true, or the most likely thing to be true
    (most cases): I should use an upsert with

    ```sql
    INSERT ... ON CONFLICT (...) DO UPDATE ....
    ```
-   If my fact is questionable, and I know that the facts in the
    database are true, or the most likely thing to be true (rare): I
    should do an upsert with

    ```sql
    INSERT ... ON CONFLICT (...) DO NOTHING
    ```
-   If the situation is more complicated and I need application logic or
    more input, such as asking a user, before deciding what to do: I
    should do a regular

    ```sql
    INSERT ...
    ```

    and check for an error.

Credit for the name "The Upsert Allegory of the Cave" goes to my
co-worker John Belisle, who compared it to Plato's Allegory of the
Cave when I shared it on our chat.