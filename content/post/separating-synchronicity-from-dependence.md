+++
title = "Separating Synchronicity from Dependence"
author = ["Matthew Krauss"]
date = 2019-06-08T00:40:00-04:00
lastmod = 2022-02-13T18:09:32-05:00
tags = ["js", "basics"]
draft = false
weight = 1005
+++

Consider the case when we have a set of asynchronous behaviors, and a
final behavior to execute in the event that they are all successful:

```js
Promise
  .all([
    Promise.resolve('a'),
    Promise.resolve('b'),
    Promise.resolve('c')
  ])
  .then(
    result => console.log('Doing the thing!', result)
  )
  .catch(
    e => console.log('it all failed, boohoo.')
  )
```

```text
Doing the thing! [ 'a', 'b', 'c' ]
```

If any of those behaviors fails, the final `.then` would not run,
which may be exactly what we want:

```js
Promise
  .all([
    Promise.resolve('a'),
    Promise.reject('no b!'),
    Promise.resolve('c')
  ])
  .then(
    result => console.log('Doing the thing!', result)
  )
  .catch(
    e => console.log('it all failed, boohoo')
  )
```

```text
it all failed, boohoo
```

Now imagine if we wanted, in the case that the second of those
asynchronous behaviors were to fail, to still go through to the final
behavior, so long as the other two succeeded. I recently saw this kind
of solution to that:

```js
Promise
  .all([
    Promise.resolve('a'),
    Promise.resolve('c')
  ])
  .then(
    result => console.log('Doing the thing!', result)
  )
  .catch(
    e => console.log('it all failed, boohoo')
  )
  .then(
    () => Promise.reject('no b!')
  )
```

```text
Doing the thing! [ 'a', 'c' ]
```

The argument was made that this change was necessary to prevent the
step `b` from stopping the process. Simply extracting the step to
prevent it from stopping the process works, certainly; but this also
prevents the second step from being executed asynchronously.

It is not necessary to give up asynchronous execution to prevent one
step from stopping the execution of later steps. Here is a better way:

```js
Promise
  .all([
    Promise.resolve('a'),
    Promise.reject('no b!').catch(e => 'b failed'),
    Promise.resolve('c')
  ])
  .then(
    result => console.log('Doing the thing!', result)
  )
  .catch(
    e => console.log('it all failed, boohoo')
  )
```

```text
Doing the thing! [ 'a', 'b failed', 'c' ]
```