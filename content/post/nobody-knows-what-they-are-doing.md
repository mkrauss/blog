+++
title = "Nobody Knows What They Are Doing"
author = ["Matthew Krauss"]
date = 2020-10-25T02:13:00-04:00
lastmod = 2022-02-13T18:09:24-05:00
tags = ["life", "philosophy"]
draft = false
weight = 1003
+++

I'm not the first to say it. Google the phrase "nobody knows what they
are doing". There are a lot of interesting takes on it.


## The Peter Principle Writ Large {#the-peter-principle-writ-large}

The Peter Principal states that in a hierarchy, people rise to their
level of incompetence. As long as one is doing well, one will be
promoted. Once one reaches the level where they are no longer
competent, they will stay there.

The Peter Principal is usually seen as a bad thing, a bane of
hierarchies, ensuring that the majority of operatives within a system
are out of their depth a majority of the time - but it can also be a
good thing. It is only when we move into a position where we are out
of our depth that we have the chance to grow. In its worst form, the
Peter Principal means a stagnant organization, bloated with
incompetency on all levels; but in its best form, it could mean a
dynamic organization, where everybody is challenged and growing at all
times.


## Ship It {#ship-it}

"The perfect is the enemy of the good."

"The good is the enemy of the good-enough."

"Ship it!"

Successful software startups are not, as a widely observed rule, those
which write the cleanest, best code, or those with the most
meticulously designed and full-featured software. They are the ones
which ship early and get a product or feature out in front of
customers first.

This is partly because they are first to market - they capture a
customer base and revenue ahead of the competition - but agile
software methodologies tell us there is another reason. By getting a
product or feature in front of customers, you can get feedback early,
and improve in later iterations. It's all about learning, and delaying
decisions.

We will never again be as ignorant as we are at this moment. We learn
all the time. Any decision we can put off without serious consequence,
therefore, we should put off - by the time we find we must make a
decision, we may know a lot more about how to make it correctly than
we do today.

If we put out a much more limited product, we have the chance to learn
even more, from experience and feedback, then if we keep it to
ourselves and keep working until it is "ready". This way, we not only
delay decisions, but maximize the information we will have when we
are finally required to make them.

The Lean Startup methodology even tells us that we should prioritize
putting out a product that will teach us more, over putting out a
product that will accomplish our other goals (like getting or keeping
customers). There is something to that. At the least, we should put a
lot of value in a product that teaches us, even if we do compromise
between learning and more immediate gains.


## Quantity Yields Quality {#quantity-yields-quality}

A ceramics teacher did an experiment. They divided a class into two
groups: the first group would be graded on quality alone; the second
would be graded on quantity alone.

By the end, which group produced the best quality pot? Perhaps
counter-intuitively, the second group. By producing more, they tried,
failed, learned, and grew, to reach better results, even though that
was not their goal.

Jeff Atwood describes this as [Quantity Trumps Quality](https://blog.codinghorror.com/quantity-always-trumps-quality/), but I would say
Quantity (under the right circumstances) Yields Quality, or to be more
cliche, Practice Makes Perfect (or at least better). With the right
support, advice, and feedback in place, the most important thing we
can do to improve at any activity is to just keep doing it. It's a
valuable lesson: don't prognosticate about the best way to do a thing;
instead, do the thing, then take what you learned and do another
thing.

That means that the best way to get good at something requires first
doing it badly!


## A Bigger Picture {#a-bigger-picture}

Take a step back. The Peter Principal, the Ship It Philosophy, and the
Quantity Experiment, are all projections of a larger truth. Whenever
we are operating at peak, when we are the expert, when we are at the
top of our game... we have already missed an opportunity to be playing
in a better game. It doesn't matter how good we are at splashing in
the wading pool - until we go into deeper water, we never learn to
really swim.

That doesn't mean we should always dive in the deep end without any
preparation or support! That's the other extreme. We cannot learn to
swim by drowning.

We need study, we need mentorship, we need preparation. We need the
support of a community who can catch us when we fall. And then we need
to take the dive, go someplace we have never been, where the water is
above our heads - someplace where, despite our preparations, we aren't
sure what to really expect - and that is when we learn.


## Continuous Learning {#continuous-learning}

It is a mistake to do something in the best way we already know how.
We should be learning from **every activity** - and that means
experimenting, to at least some extent. It doesn't matter how critical
a task is - if we fall into the trap of considering immediate success
more important than the constant process of learning, we will not
succeed at the next critical task; we will stagnate; we will go around
in ever-more-perfect circles, never really moving forward to the next
big thing.


## Nobody (doing anything significant) Knows What They Are Doing {#nobody--doing-anything-significant--knows-what-they-are-doing}

I will propose a radical statement: any time people know what they are
doing, they are not doing anything significant. If everybody around
you seems incompetent, then everybody around you has an opportunity to
grow.

Because we live in a capitalism, we are always pushing each other to
work harder or smarter or both, just to keep up, just to survive, and
that can be a disastrously bad thing. However, if we know it's
happening, we can at least try to mitigate the damage, and make the
most of the opportunity.

When we are lucky enough to be able to work on something that is not
for our survival, but for the public good, we can push ourselves to
deeper waters, with proper preparation and support.

Any time we know what we are doing, we are not doing anything
significant. That's fine - not everything has to be significant. But
if you want to do something big - either objectively big in the world,
or just big for you - then you have to do something new.