+++
title = "Emacs enhaced goto-line"
author = ["Matthew Krauss"]
date = 2020-12-12T14:00:00-05:00
lastmod = 2022-02-13T18:09:21-05:00
draft = false
weight = 1002
+++

The Emacs command `goto-line`:

-   Will prompt for a line number, and go to that line.
-   Given a numeric prefix argument, it will go directly to that line,
    without prompting.
-   Called as a function, you can pass a line number.

So a long time ago, I must have added this little function from
somewhere to my Emacs. I actually forgot that this wasn't the default
behavior:

```elisp
(global-set-key [remap goto-line] 'goto-line-with-feedback)

(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input"
  (interactive)
  (unwind-protect
      (progn
        (linum-mode 1)
        (goto-line (read-number "Goto line: ")))
    (linum-mode -1)))
```

I don't know where I got this. If you do, please tell me so I can
credit the source.

It's nice. It simply shows line numbers, prompts for a line, calls
that same `goto-line` to go there, and then turns off line numbering
again, so that you have visual feedback as you enter a line number. As
a replacement for the original `goto-line`, though, it loses the
ability to accept a prefix argument. (It also can't accept a line
number when called as a function, but that's fine, because the
original `goto-line` is still available, of course.) Also, if you
already have `linum-mode` enabled, it will turn it off, rather than
preserving that. These minor issues are not noticeable to me at all.

In Emacs, you can narrow the buffer to work with a subset of the total
buffer. When the buffer is narrowed, most commands act like the subset
you are narrowed to is the entire buffer. This is quite handy. Here,
for reference, are the standard keybindings for narrowing commands:

C-x n d
: narrow-to-defun: narrows to whatever the current mode
    considers a defun or function

C-x n n
: narrow-to-region: narrows to the selected region

C-x n p
: narrow-to-page: narrows to the current page, in a
    document with page delimiters

C-x n w
: widen: removes any narrowing

Recently, I noticed that `goto-line-with-feedback` doesn't handle line
numbers correctly in a narrowed buffer. There are two possible ways to
be correct here: it could show absolute line numbers, and interpret
input as absolute line numbers; or it could show relative line
numbers, and interpret input as relative line numbers. What it
actually does is show relative line numbers, and interpret input as an
absolute line number.

So after some fiddling, I changed it to this:

```elisp
(global-set-key [remap goto-line] 'goto-line-with-feedback)

(defun prompt-for-line-with-feedback (absolutep)
  "Show line numbers and prompt for one. Use absolute line
numbers if absolutep is true; otherwise use numbers relative to
the current narrowing."
  (let ((previous-linum-mode linum-mode)
        (linum-format (if absolutep
                          (lambda (line)
                            (format "%5d" (+ line (count-lines 1 (point-min)))))
                        linum-format)))
    (unwind-protect
        (progn
          (linum-mode 1)
          (read-number "Goto line: "))
      (if (not previous-linum-mode) (linum-mode 0)))))

(defun goto-line-with-feedback (line absolutep)
  "Optionally prompt for a line number, and go to that line.

Given a numeric prefix argument, go directly to that line. Given
a non-numeric prefix argument, prompt for an absolute line
number. Given no prefix argument, prompt for a line number
relative to the current narrowing."
  (interactive
   (list (if (numberp current-prefix-arg)
             current-prefix-arg
           (prompt-for-line-with-feedback current-prefix-arg))
         current-prefix-arg))
  (if absolutep
      (goto-line line)
    (goto-char (point-min))
    (forward-line (1- line))))
```

This has a few improvements:

-   As a function, it accepts both the `line` number to go to, and an
    `absolutep` predicate indicating if it should use absolute or
    relative line numbers, in a narrowed buffer.
-   As an interactive command:
    -   if given a numeric prefix (e.g. `M-5`, or `C-u 5`), like the
        original `goto-line` it will go directly to that line (using
        absolute line numbers);
    -   if given a non-numeric prefix (e.g. `C-u`), it will display absolute line
        numbers and prompt for input, which will be interpreted as an
        absolute line number;
    -   if given no prefix, it will display relative line numbers, and
        prompt for input, which will be interpreted as a relative line
        number.
-   In either case, if line numbering is already enabled, it will be
    left in the same state afterwards.

The thinking is that if you just want to go to a line visually, `M-g
M-g` will display convenient, short, relative line numbers in your
narrowed buffer; if you want to go to an absolute line, such as a
source line given by debug output, `C-u M-g M-g` will let you enter
that line, without even looking at the displayed line numbers, if you
like; or you can do the same by holding down the Meta key and typing,
for instance, `5 2 g g` (explicitly, `M-5 M-2 M-g M-g`) to go directly
to that line without a prompt.