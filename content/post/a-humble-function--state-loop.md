+++
title = "A humble function - state_loop"
author = ["Matthew Krauss"]
date = 2013-04-15T22:46:00-04:00
lastmod = 2022-02-13T18:09:37-05:00
tags = ["js", "fp", "clever"]
draft = false
weight = 1007
+++

Today I needed a Javascript function to hold a toggle state and do one
of two things, in alternation. Rather than just hard code a closure
over a boolean, I decided to generalize the problem and make a
function generator that can create a function to cycle between any
number of states (where each state is defined as a function to enter
it).

```javascript
function state_loop() {
    /*
     * Take a variable number of functions, and return a function which
     * calls them in sequence, and starts over when they are
     * exhausted. Good for quick toggles and other things.
     */

    var states = arguments;
    var i = states.length - 1;

    return function() {
        return states[i = i < states.length - 1 ? i + 1 : 0]();
    };
}
```

An example of usage:

```javascript
some_element.onclick = state_loop(fn_to_turn_on, fn_to_turn_off);
```

**Of course** one should not usually just set a handler like that - it's
an example. Also, note that it can take any number of functions, not
just two.

The point is that Javascript is way too much power and fun, and should
be banned from any project that wants to develop a large, error-prone,
slow-to-change code base.

No, the real point is this: I am sure that this function must be a
known, common technique in FP, but somehow I have missed it. This
cannot be my invention. I am hoping someone will read this and let me
know what this is usually called.

Thanks in advance!