+++
title = "Arbitrarily ordered relational data"
author = ["Matthew Krauss"]
date = 2014-10-26T00:00:00-04:00
lastmod = 2022-02-13T18:09:34-05:00
tags = ["sql"]
draft = false
weight = 1006
+++

## The Problem {#the-problem}

You have a relational database. You have data which you would like to
keep in this database. You have the need to allow a user to manually
define the order of the data, without regard to a distinct attribute
of the data.

Examples:

User stories in scrum backlog
: A really pure example. The business
    users need to be able to arbitrarily prioritize the stories. There
    is nothing to base this on other then the interactive process of a
    user saying "this is more important than that, but less important
    than the other thing".

Chapters of a book
: Chapters are often numbered, and it's not
    unreasonable to require they be given a definitive numbering, even
    if that isn't reflected in the actual chapter title. However, it
    seems like a good example where, depending on application, you may
    want to re-order arbitrarily.

Contacts at an organization
: An excellent example for its
    potential complexity. We may want an arbitrary ordering for all or
    some of the most important contacts at an organization. We may want
    to keep different orderings per organization. We may want to allow
    different users to have their own ordering(s) per organization. We
    also may want to retain the ability to order contacts by attributes,
    such as alphabetical, or by the number of sales made through that
    contact, or whatever.


## Thoughts on Efficiency {#thoughts-on-efficiency}

A lot of objections to different approaches might be based in
efficiency. I'll mention these where appropriate, however, I want to
de-emphasize them from the start.

It is premature optimization, perhaps very premature. I cannot off
hand imagine a scenario where we would be ordering a large number of
tuples.

For instance, we may order the chapters of each book in our database,
but we don't have to order all the chapters as a whole. Our ordering
operations would be confined to the chapters whose (presumably
indexed) "book" key match the book being ordered. The same logic
applies to the other examples being considered here.

In fact, we can more generally note that arbitrary ordering is hard. I
don't mean it is a hard technical challenge to allow it, but that the
actual process of a human being going through, say, user stories and
ranking the importance of each one is a lot of trouble. We are really
very unlikely to want to apply arbitrary ordering to large sets, and
therefor, should probably not worry much about how our arbitrary
ordering solution scales.


## Linked List {#linked-list}

We may think to add an attribute that points to the previous or next
tuple. The linked list is a really handy data structure in a lot of
contexts, and application programmers understand and like this
structure.

A relational database doesn't have any easy way to order tuples based
on a linked list. You might use a recursive common table expression to
do it, but this is unlikely to be very efficient, and more
importantly, it is fairly complex. Still, it might be a viable
solution given a view that hides this complexity.

Data integrity is fairly easy. The combination of a categorizing
foreign key (e.g. "book" for a chapter, "project" for a story,
"organization" for a contact) plus "previous" or "next" can be a
foreign key to the same relation.

It is very compelling to be able to directly update a tuple setting,
for instance, it's "after" attribute to the tuple you want to move it
after. You could even have a writable view that lets you set an
"after" or a "before", and of course, you'll want to settle on a
signal value for the first or last position, possibly NULL.


## Assign an integer ordering {#assign-an-integer-ordering}

One possibility is to add an ordering attribute.

The obvious type is integer. Our first instinct may be to keep this a
dense attribute, so that each item follows the previous. If we do
this, than every time we need to insert an item, we need to re-order
every following item.

Another way to manage integer ordering is a sparse attribute. Choose a
big integer data type. When entering the first, say, user story, place
it at the middle of the number space, e.g., MAXINT/2. When inserting
at the beginning, use MIN(ordering)/2. At the end, use (MAXINT +
MAX(ordering)) / 2. Inserting between two tuples, use the average of
their orderings.

It will be a long time before you hit a "collision" - a situation
where you are attempting to insert between two tuples with consecutive
"ordering" attributes. Keep in mind that ordering does not need to be
shared between categorizing tuples - the ordering of chapters of one
book is not related to that of another. For most real-world
applications I'll bet it would never happen. However, you still will
need to be prepared.

In the event of a collision, or perhaps as a regular maintenance, you
may want to normalize the ordering of all tuples. In case of
collision, you would just re-order the given book, project, or
whatever category was being updated, or alternately, just change the
minimum number of ordering values to do what you need. As routine
maintenance you may want to re-order everything.

This may be faster for reads. It's probably slower for writes.

It seems much more complex and not worth it to me, even if you ignore
re-ordering.


## Assign a float or decimal ordering {#assign-a-float-or-decimal-ordering}

Using float or decimal gives you the ability to split the difference
between two ordering values much more deeply. Instead of reordering
when we need to insert between, say, 6 and 7, we can just go to 6.5,
then 6.75, 6.875, etc.

I would definitely avoid floats. If you are not aware of the issues
with floating point math errors, you owe it to yourself to learn about
them. With other data types we might eventually hit a point where we
have to reorder, but with floats, we won't necessarily know - suddenly
the math will just stop working and our ordering will become
unreliable. It may be unlikely to hit this point, but why risk it?

Decimal is a better option, giving us a huge amount of precision. If
you combine this enhanced ability to split the difference with the
sparse population technique discussed for integer ordering you
dramatically decrease the likelihood that you will ever have to
reorder.

However, unless you are willing to say "Eh, with this big a number
space, it will never happen", you really aren't gaining much here. The
most significant win we could have in terms of simplification is to
not have to worry about reordering at all. Not having to do it as
often is no less implementation complexity.

Any system which requires us to handle reordering is going to make it
very hard, if not impossible, to hide our complexity behind writable
views.


## Assign a text ordering {#assign-a-text-ordering}

Text allows us basically unlimited capacity to "split the difference".
We never have to reorder.

Imagine starting off sparsely populating your text attribute with
uppercase alphabetical characters - start from the middle of the
alphabet, "N", and split the difference until you hit what would be a
collision. Let's say we are trying to insert between "C" and "D" -
instead of reordering, we can just use "CN" and then start splitting
to "CF" and "CC". When we need to split between 2-character orderings,
we just go to three characters - "CCN".

Since text holds unlimited characters, we won't ever have to reorder
in even the most extreme cases. The worst thing that will happen is we
may have some **very** large ordering values, and an unwieldy index.

This is a big simplicity win over the numeric options, and I am pretty
comfortable living with that potential performance hit. Remember that
arbitrary ordering is hard and is unlikely to be done on large sets or
with large amounts of changes in the real world. We may never go
beyond two characters, keeping things nice and efficient, but with the
text field we know that it won't ever blow up on us.

Since there is no need to reorder, we can hide the complexity in a
writable view, and unlike the linked list, we don't need common table
expression recursion to return the tuples ordered - we just say: ORDER
BY "ordering".


## Keep order in an array {#keep-order-in-an-array}

Unlike the other options, this places our ordering attribute not on
the individual tuples being ordered, but on the category tuple. Each
book tuple, for instance, has a chapters array which orders the
chapters. I find this compelling. The association of a chapter to a
book seems naturally a property of that chapter, but the ordering of
chapters within the book seems more naturally associated with the book
on the whole.

It would still be best if each chapter also kept a key back to the
book - that's proper relational after all, and can be indexed. The
point of putting chapter keys in an array on their respective book is
not to say "this chapter belongs to this book" but to say "within this
book, the chapters appear in this order".

The major drawback here is that an array, at least in PostgreSQL, is
not a linked list. It represents an intrinsic ordering. but is not
really less clumsy for re-ordering than a relation.


## Conclusion??? {#conclusion}

The usual structuring for an article like this is to start from the
worst solution and work your way forward to the best. That is exactly
what I meant to do, but wound up getting it wrong. The more I consider
the array solution, the worse it sounds; the more I consider the
linked list, the better it sounds.

Of course, common table expressions are a must for this solution, and
I haven't tried building out a working example yet, so the outlook is
hazy at the moment, but I am definitely leaning toward the linked
list.

Another note: I did a fair bit of web searching to see what the expert
opinions were, and I have not found any analysis as solid as this one.
Please note that this one is really not solid at all. It's an issue
that just doesn't seem to have a good answer.