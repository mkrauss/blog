+++
title = "Enhanced Lisp notation"
author = ["Matthew Krauss"]
date = 2012-11-18T02:24:00-05:00
lastmod = 2022-02-13T18:09:40-05:00
tags = ["lisp", "thoery"]
draft = false
weight = 1009
+++

I have been mulling over some ideas for a new Lisp notation. Old
school Lispers would probably frown at this, but I think it could
serve to lower the bar to entry for new programmers, and increase
readability for even old hands once the adjustment was made.


## Background {#background}

Traditionally, a list is notated as a sequence of elements separated
by white space, where each element is either an atom or a
parenthesized list. An atom may include white space, but only under
defined conditions - typically white space will terminate an atom.

I'm not sure if implementations treat the parentheses as part of the
list they surround or not, but would argue they should not be
considered so. After all, a file full of Lisp code is a list itself,
and the file does not have a pair of parentheses around it.

It will be useful to think of the list as defined by the presence of
the white space separations, rather than the parentheses.


## A small addition {#a-small-addition}

Now consider allowing comma and semicolon to also separate list
elements. For readability, it would be any sequence of white space
which includes a single comma or semicolon. Further, let's give the
comma a higher binding precedence than pure white space, and the
semicolon a lower precedence. This gives us three separators, in this
order of precedence:

1.  a single comma, allowing any additional space on either side
2.  at least one character of white space, allowing any amount of
    additional space
3.  a single semicolon, allowing any additional space on either
    side


### Commas and semicolons? {#commas-and-semicolons}

Yes, I know - the comma and the semicolon both already have an
important use. Let's just ignore that for now and continue - we'll
take a look at them later.


## Just one more thing {#just-one-more-thing}

Bear with me here, we will see the reasons for these things soon, but
first one last addition. Let's allow curly braces to delimit a part of
the list syntactically, but place the elements of that list directly
in the context list. In other words:

```text
a,b c,d; {e,f g,h; i,j k,l}; m,n o,p
```

would be equivalent to:

```text
(((a b)
  (c d))
 ((e f)
  (g h))
 ((i j)
  (k l))
 ((m n)
  (o p)))
```

It is important that the curly braces do not create new lists. The
significance of this will be revealed below, when we look at a LET
form.


## Clean it up {#clean-it-up}

Consider the following little piece of code:

```text
(princ "foo")
```

This is the typical way we show Lisp code, but note that the
parentheses are not needed for this one expression in isolation -
since we have decided the list is formed by the spaces rather than
the parentheses, we could just as well say:

```text
princ "foo"
```


## A healthy semicolon {#a-healthy-semicolon}

Of course, in the context of a program the parentheses are needed to
separate this from the rest of the code, like the following:

```text
(princ "foo")
(princ "bar")
```

We would never get away with this:

```text
princ "foo"
princ "bar"
```

(That way lies the madness of Python!)

However, if a semicolon also forms a list, but at a lower binding
precedence than a space, we could do this:

```text
princ "foo";
princ "bar"
```

Simple so far! This would represent the exact same code structure as
the more traditional version.


## Brace yourself {#brace-yourself}

Let's try something else:

```text
(let ((sum (+ arg1 arg2)) (product (* arg1 arg2)))
  (format t "sum: ~a, product: ~a" sum product)
  (princ "The stuff has been printed"))
```

This could be represented like so:

```text
let (sum (+ arg1 arg2); product (* arg1 arg2)) {
  format t "sum: ~a, product: ~a" sum product;
  princ "The stuff has been printed"};
```

Note that we have done nothing to invalidate the traditional
notation; the above two snippets would be functionally identical.

It is possible for us to start a new list of semicolon separated forms
and include those at the top level of the `let` form because of the
curly braces. If we used parentheses here, the `let` body would be a
sub-list, which would of course not work; if we left out the
delimiters entirely, the first semicolon after the `format` statement
would end the `let` form.


## Comma Chameleon {#comma-chameleon}

Of course, real code is usually in functions:

```text
(defun myfun (arg1 arg2)
  (princ "Begin function")

  (let ((sum (+ arg1 arg2)) (product (* arg1 arg2)))
    (format t "sum: ~a, product: ~a" sum product)
    (princ "The stuff has been printed"))

  (princ "End of function"))
```

We can modify this too:

```text
defun myfun (arg1 arg2) {
  princ "Begin function";

  let (sum (+ arg1 arg2); product (* arg1 arg2)) {
    format t "sum: ~a, product: ~a" sum product;
    princ "The stuff has been printed"};

  princ "End of function"};
```

But where could commas be useful to clean this up? Remember, they bind
at a higher precedence than just white space:

```text
defun myfun arg1, arg2 {
  princ "Begin function";

  let (sum (+ arg1 arg2); product (* arg1 arg2)) {
    format t "sum: ~a, product: ~a" sum product;
    princ "The stuff has been printed"};

  princ "End of function"};
```

Note that we haven't, for instance, replaced the summation form with a
comma version, like the following less-readable version:

```text
defun myfun arg1, arg2 {
  princ "Begin function";

  let (sum +, arg1, arg2; product *, arg1, arg2) {
    format t "sum: ~a, product: ~a" sum product;
    princ "The stuff has been printed"};

  princ "End of function"};
```

That would be legitimate under this notation, but would decrease
rather than increase readability.


## Homoiconicity {#homoiconicity}

I think it's important to stop and remember, here, that this is **not**
a C-like syntax. It **looks** C-like, and has some of the readability
and familiarity of C-like syntax, but it is full homoiconic and
directly isomorphic to traditional Lisp.


## Now about your comments and back-quote interpolation {#now-about-your-comments-and-back-quote-interpolation}

I'm holding your precious syntactic sugar hostage for... one **million**
dollars! MUAHAHAHAHAH!

OK, maybe not. I will present some ideas to deal with this, but I
don't find any of them completely satisfactory, and would love to hear
other ideas.


### Commas {#commas}

My first suggestion is that the traditional comma be replaced
with another symbol. Comma never seemed to make much sense in
that role. Also, this is such a major mental shift, we may as
well go for broke.

Another option is to use another symbol in place of it for the
high-binding list separator, although I don't know what would be
as readable for that. The comma just works great as a separator.

Finally, we could let the comma serve both roles. The back-quote
list interpolation operator can be followed by white space, but
it seems like most people do not do that; our new notation could
require that the comma used as a separator be followed by white
space, and that the interpolation comma be followed immediately
by it's argument.


### Semicolons {#semicolons}

Again, I think the semicolon was born for this new role. Although
the new notation could use a different character, it just
wouldn't have the same psychological effect.

We could assign a new comment character. Suggestions?

Or we could insist that only a doubled semicolon is a comment
starter.


## Conclusion {#conclusion}

There is no conclusion to this yet.

I believe that all of this could possibly be implemented with reader
macros, but reader macros are one area of Lisp that I don't know
enough about yet, so I could be mistaken.

I don't actually know if any of this is a good idea. I am comfortable
in Lisp as it is, as are most existent Lisp programmers. I don't have
any immediate plans to implement something like this. Mostly I just
wrote this piece because the ideas keep running through my mind, and I
had to get it out.

I'd love to hear anybody's thoughts on the subject, whether for,
against, or other.