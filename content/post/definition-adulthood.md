+++
title = "Definition: adulthood"
author = ["Matthew Krauss"]
date = 2012-10-02T00:18:00-04:00
lastmod = 2022-02-13T18:09:41-05:00
tags = ["life", "music"]
draft = false
weight = 1010
+++

So I saw this video of Kimya Dawson's song "I Like Giants" on Youtube.
The video features adults dressed in some silly outfits dancing
around. Some of the comments were less than flattering.

I've come to the realize over the years that most people are afraid of
whatever they perceive as childish, innocent, or even just good and
selfless. When they see these things, they hide behind sneering or
condescending comments. They're afraid, I think, that if they don't
disapprove strongly enough, they'll be tainted with the same
perception. This seems similar to how people who persecute gay people
are often closeted homosexuals - they are hiding from themselves.

This is easy to see when people attack something saying it's dorky,
lame, bleeding-heart, or other labels; but one of the more insidious
slanders is to say something is childish, to say "grow up" or "act
your age". It's harder to see what is wrong here - shouldn't we be
adults?.

When we are children, those of us who are lucky have good parents or
other adults who look after us, and correct our behavior. They stop us
from licking power outlets or kicking our siblings, and they make sure
we do what is needed to grow up safe and healthy.

Then, we become grownups. But what does that mean, to be a grownup?
Does it mean you should not enjoy silliness anymore? Is something
necessarily lost? I propose that being a grownup means gaining, not
losing, something. It means you are your own parent. A grownup
monitors his or her own behavior - that simple.

Now, a good parent lets their child get silly, run around and be a kid
sometimes. It can't all be homework and chores! We all need our ways
to have fun, and letting yourself unwind - with a silly dance,
role-playing at the Renaissance Festival, playing a video game, or
whatever other harmless activity - that's just being a good parent to
yourself.

The people who are afraid of these things - they do other things. Some
of them drink to the point of blacking out, or abuse other drugs. Some
of them yell at co-workers or treat their spouses with verbal or
physical abuse. Some of them just say hurtful things about the
creative efforts of people they don't know. **These** are the people who
have not grown up. They still need a parent to stop them, to tell them
not to do things that hurt themselves, or others.