+++
title = "About"
author = ["Matthew Krauss"]
lastmod = 2019-02-02T10:44:17-05:00
draft = false
weight = 1001
+++

## whoami {#whoami}

-   Matthew Krauss
-   Software developer
-   Armchair philosopher


## host {#host}

This blog is written in [Org](https://orgmode.org/), using [GNU Emacs](https://www.gnu.org/software/emacs/) running on [Debian](https://www.debian.org/)
[GNU](https://www.gnu.org/)/[Linux](https://www.linux.com/), exported to [Hugo](https://gohugo.io/) with [Ox-Hugo](https://github.com/kaushalmodi/ox-hugo), and hosted at [GitLab](https://about.gitlab.com/) using
the [Beautiful Hugo](https://pages.gitlab.io/hugo/) template. That's a lot of wonderful free software
that makes it possible for me to ramble into the void, and I'm
probably missing a bunch.
