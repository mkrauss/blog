+++
title = "Front"
author = ["Matthew Krauss"]
lastmod = 2019-02-01T19:59:13-05:00
draft = false
weight = 1001
+++

"Do not try to balance the parentheses — that's impossible. Instead,
only try to realize the truth."

"What truth?"

"That there are no parentheses."

"There are no parentheses..."

"Then you will see that it is not the parentheses that balance — it is
only yourself."
